#!/bin/bash

set -euo pipefail

# log info to stderr
log_info() {
  echo 1>&2 "$@"
}

log_info "ARGOCD_APP_PARAMETERS: $ARGOCD_APP_PARAMETERS"

# fetch secret values from managing k8s cluster
SECRET_DATA_VALUES=""
if [ -n "${PARAM_VALUES_SECRET_REF:-}" ] ; then
  IFS=/ read -r namespace name key <<<"$PARAM_VALUES_SECRET_REF"
  sa=/var/run/secrets/carvel-plugin-serviceaccount

  SECRET_DATA_VALUES=$(
    kubectl \
      --server=https://kubernetes.default.svc \
      --token="$(cat "$sa/token")" \
      --certificate-authority="$sa/ca.crt" \
      get secret -n "$namespace" "$name" -o jsonpath="{.data.${key}}" | base64 -d
  )
fi

args=( )

# generate -f arguments (makes sure filenames can contain spaces)
while IFS=$'\n' read -r file ; do
  args+=("-f" "$file")
done < <(jq -r '. // [] | (.[] | select(.name == "files").array[]) // "."' <<<"$ARGOCD_APP_PARAMETERS")

# generate --data-values-file arguments
while IFS=$'\n' read -r file ; do
  args+=("--data-values-file" "$file")
done < <(jq -r '. // [] | .[] | select(.name == "data-values-files").array[]' <<<"$ARGOCD_APP_PARAMETERS")

log_info "args: " "${args[@]}"

ytt \
  "${args[@]}" \
  --data-values-file <(echo "${PARAM_DATA_VALUES:-}") \
  --data-values-file <(echo "${SECRET_DATA_VALUES}") 
