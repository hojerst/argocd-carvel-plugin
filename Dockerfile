FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c AS downloads

# renovate: datasource=github-releases depName=carvel/ytt
ARG YTT_VERSION=0.49.0

WORKDIR /work

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# hadolint ignore=DL3018
RUN apk --no-cache add curl
RUN ARCH="$(uname -m | sed -e s/x86_64/amd64/ -e s/aarch64/arm64/)" \
 && curl -Ls "https://github.com/carvel-dev/ytt/releases/download/v${YTT_VERSION}/ytt-linux-$ARCH" -o ytt

FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# hadolint ignore=DL3018
RUN apk --no-cache add kubectl jq bash

COPY --from=downloads --chmod=0755 /work/ytt /usr/bin
COPY plugin.yaml /home/argocd/cmp-server/config/plugin.yaml
COPY --chmod=0755 argocd-carvel-plugin.sh /usr/bin/argocd-carvel-plugin
