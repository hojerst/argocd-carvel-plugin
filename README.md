# ArgoCD Carvel Plugin

This is a Carvel plugin for ArgoCD for rendering YTT templates.

## Installation

### Patch argocd-repo-server deployment

```yaml
---
spec:
  template:
    spec:
      volumes:
        - secret:
            secretName: carvel-plugin-token
          name: carvel-plugin-token
        - emptyDir: {}
          name: carvel-plugin-tmp
      containers:
        - name: carvel-plugin
          image: index.docker.io/hojerst/argocd-carvel-plugin:v1.0.0
          command: [/var/run/argocd/argocd-cmp-server]
          securityContext:
            runAsNonRoot: true
            runAsUser: 999
          volumeMounts:
            - mountPath: /var/run/argocd
              name: var-files
            - mountPath: /home/argocd/cmp-server/plugins
              name: plugins
            - mountPath: /tmp
              name: carvel-plugin-tmp
            - mountPath: /var/run/secrets/carvel-plugin-serviceaccount
              name: carvel-plugin-token
```

### Create a Service Account for the plugin and allow it to read secrets

Note: The following Role allows to access secrets in the ArgoCD namespace only. However, you can modify the Role to allow access to secrets in other namespaces as well. (i.e. use a ClusterRole / ClusterRoleBinding instead)

```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: carvel-plugin
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: carvel-plugin
rules:
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["get", "list"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: carvel-plugin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: carvel-plugin
subjects:
  - kind: ServiceAccount
    name: carvel-plugin
---
apiVersion: v1
kind: Secret
metadata:
  name: carvel-plugin-token
  annotations:
    kubernetes.io/service-account.name: carvel-plugin
type: kubernetes.io/service-account-token
```

## Usage

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-app
  namespace: argocd
spec:
  destination:
    name: in-cluster
    namespace: demo
  source:
    repoURL: https://example.org/repo.git
    targetRevision: HEAD
    plugin:
      name: carvel-v1
      parameters:
        - name: files
          array: # runs `ytt -f folder1 -f folder2 -f file.yml`
            - folder1
            - folder2
            - file.yml
        - name: data-values # pass content as `--data-values-file` to `ytt`
          string: |
            datavalue: "my value"
        - name: data-values-files # pass repository files as `--data-values-file` to `ytt`
          array:
            - prod/values.yml
        - name: data-values-secret-ref # pass content as `--data-values-file` to `ytt`, but fetch it from a secret (higher priority than `data-values`)
          string: argocd/my-secret/values.yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
  namespace: argocd
stringData:
  values.yaml: |
    message: "my secret message"
```

Note: You can also manage the actual secret with something like [SealedSecrets](https://sealed-secrets.netlify.app/) or [ExternalSecrets](https://external-secrets.io).
